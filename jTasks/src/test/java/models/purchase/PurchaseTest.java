package models.purchase;

import exeptions.NegativeArgumentException;
import exeptions.NonPositiveArgumentException;
import models.Byn;
import models.Product;
import org.junit.Assert;
import org.junit.Test;

public final class PurchaseTest {


    @Test(expected = NonPositiveArgumentException.class)
    public void createObjectWithIncorrectValue() {
        Product product = new Product("meat", "2.01");
        new Purchase(product, 0);
    }

    @Test(expected = NonPositiveArgumentException.class)
    public void createObjectWithIncorrectValue2() {
        Product product = new Product("meat", new Byn(100));
        new Purchase(product, -1);
    }

    @Test
    public void getCostTest1() {
        Purchase purchase = new Purchase(new Product("meat", "2.01"), 2);
        Byn expected = new Byn(402);
        Byn actual = purchase.getCost();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCostTest2() {
        Purchase purchase = new Purchase(new Product("meat", "0.01"), 2);
        Byn expected = new Byn(2);
        Byn actual = purchase.getCost();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCostTest3() {
        Purchase purchase = new Purchase(new Product("meat", "10.00"), 1);
        Byn expected = new Byn(1000);
        Byn actual = purchase.getCost();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void equalsTest1() {
        Purchase purchase = new Purchase(new Product("meat", "2.01"), 1);
        Purchase purchase1 = new Purchase(new Product("meat", "2.02"), 1);

        Assert.assertNotEquals(purchase, purchase1);
    }

    @Test
    public void equalsTest2() {
        Purchase purchase = new Purchase(new Product("meat", "2.01"), 1);
        Purchase purchase1 = new Purchase(new Product("meat", "2.01"), 1);

        Assert.assertEquals(purchase, purchase1);
    }

    @Test
    public void hashcodeTest1() {
        Purchase purchase1 = new Purchase(new Product("meat", "2.01"), 1);
        Purchase purchase2 = new Purchase(new Product("meat", "2.02"), 1);

        Assert.assertNotEquals(purchase1, purchase2);
    }

    @Test
    public void hashcodeTest2() {
        Purchase purchase1 = new Purchase(new Product("meat", "2.01"), 1);
        Purchase purchase2 = new Purchase(new Product("meat", "2.01"), 2);

        Assert.assertEquals(purchase1, purchase2);
    }
}