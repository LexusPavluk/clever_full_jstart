package models.purchase;

import models.Byn;
import models.Product;
import org.junit.Assert;
import org.junit.Test;

public class PercentDiscountPurchaseTest {

    @Test
    public void getCostTest1() {
        PercentDiscountPurchase purchase =
                new PercentDiscountPurchase(new Product("meat", "2.01"), "10.0");
        Byn expected = new Byn("18.09");
        Byn actual = purchase.getCost();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCostTest2() {
        PercentDiscountPurchase purchase =
                new PercentDiscountPurchase(new Product("meat", "1.01"), 2);
        Byn expected = new Byn(202);
        Byn actual = purchase.getCost();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCostTest3() {
        PercentDiscountPurchase purchase =
                new PercentDiscountPurchase(new Product("meat", "2.22"), 6);
        Byn expected = new Byn(1199);
        Byn actual = purchase.getCost();

        Assert.assertEquals(expected, actual);
    }

}