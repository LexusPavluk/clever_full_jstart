package models;

import org.junit.Assert;
import org.junit.Test;

public final class ProductTest {

    @Test(expected = NullPointerException.class)
    public void createObjectWithNullArg() {
        Product data = null;
        new Product(data);
    }

    @Test(expected = NullPointerException.class)
    public void createObjectWithNullName() {
        String name = null;
        new Product(name, new Byn(1));
    }

    @Test(expected = NullPointerException.class)
    public void createObjectWithNullByn() {
        String name = "milk";
        Byn byn = null;
        new Product(name, byn);
    }

    @Test
    public void compareIdenticalProducts() {
        String data1 = "meat";
        String data2 = "1.00";
        Product expected = new Product("meat", new Byn(100));
        Product actual = new Product(data1, data2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void equalsNotIdenticalProducts() {
        Product product = new Product("milk", "3.14");
        Product product1 = new Product("milk1", "3.14");

        Assert.assertNotEquals(product, product1);
    }

    @Test
    public void equalsIdenticalProducts() {
        Product product = new Product("milk", "3.14");
        Product product1 = new Product("milk", "3.14");

        Assert.assertEquals(product, product1);
    }

    @Test
    public void hashCodeNotEqualsProducts() {
        Product product = new Product("milk", "3.14");
        Product product1 = new Product("milk", "2.17");

        Assert.assertNotEquals(product.hashCode(), product1.hashCode());
    }

    @Test
    public void hashCodeEqualsProducts() {
        Product product = new Product("milk", "3.14");
        Product product1 = new Product("milk", "3.14");

        Assert.assertEquals(product.hashCode(), product1.hashCode());
    }

    @Test
    public void testToString() {
        String expected = "milk;3.14";
        Product product = new Product("milk", "3.14");
        Assert.assertEquals(expected, product.toString());
    }
}