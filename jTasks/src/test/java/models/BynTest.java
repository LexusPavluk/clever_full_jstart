package models;

import exeptions.NegativeArgumentException;
import exeptions.PatternArgumentException;
import exeptions.WrongArgumentException;
import org.junit.Assert;
import org.junit.Test;

public final class BynTest {
    @Test(expected = NegativeArgumentException.class)
    public void createBynWithNegativeValue() {
        new Byn(-1);
    }

    @Test(expected = WrongArgumentException.class)
    public void createBynWithNegativeCoins() {
        new Byn(1, -1);
    }

    @Test(expected = WrongArgumentException.class)
    public void createBynWithNegativeRubs() {
        new Byn(-11, 11);
    }

    @Test(expected = PatternArgumentException.class)
    public void createBynWithNegativeStringValue() {
        new Byn(new Byn("-123.00"));
    }

    @Test(expected = NullPointerException.class)
    public void createBynFromNullString() {
        String a = null;
        new Byn(a);
    }

    @Test(expected = PatternArgumentException.class)
    public void createBynFromIncorrectString() {
        String a = "11_";
        new Byn(a);
    }

    @Test(expected = PatternArgumentException.class)
    public void createBynFromIncorrectStringNumber() {
        String a = "822256";
        new Byn(a);
    }

    @Test
    public void createBynFromCorrectString() {
        String a = "1.50";
        Byn byn = new Byn(a);
        Assert.assertNotNull(byn);
    }

    @Test
    public void createBynFromRubsCoins() {
        int rubs = 1;
        int coins = 2;
        Byn byn = new Byn(rubs, coins);
        Assert.assertNotNull(byn);
    }

    @Test
    public void createBynWithIntValue() {
        int coins = 100;
        Byn byn = new Byn(coins);
        Assert.assertNotNull(byn);
    }

    @Test
    public void createBynWithEmptyConstructor() {
        Byn byn = new Byn();
        Assert.assertNotNull(byn);
    }

    @Test
    public void sumOtherByn() {
        Byn byn = new Byn(100);
        Byn expected = new Byn(200);
        Byn actual = byn.sum(byn);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void subEqualByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(100);
        Byn expected = new Byn();
        Byn actual = byn.sub(byn1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void subSmallerByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(99);
        Byn expected = new Byn(1);
        Byn actual = byn.sub(byn1);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = ArithmeticException.class)
    public void subBiggerByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(102);
        byn.sub(byn1);
    }

    @Test
    public void multiplyByTwo() {
        Byn byn = new Byn(100);
        Byn expected = new Byn(200);
        Byn actual = byn.multiply(2);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NegativeArgumentException.class)
    public void multiplyToNegativeNumber() {
        Byn byn = new Byn(100);
        Byn actual = byn.multiply(-1);
    }

    @Test
    public void compareToSmallerByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(99);
        int expected = 1;
        int actual = byn.compareTo(byn1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void compareToEqualsByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(100);
        int expected = 0;
        int actual = byn.compareTo(byn1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void compareToBiggerByn() {
        Byn byn = new Byn(100);
        Byn byn1 = new Byn(101);
        int expected = -1;
        int actual = byn.compareTo(byn1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void equalsToNotEqual() {
        Byn byn1 = new Byn(1, 2);
        Byn byn2 = new Byn("1.08");

        Assert.assertNotEquals(byn1, byn2);
    }

    @Test
    public void equalsToEqualBYNs() {
        Byn byn1 = new Byn("1.08");
        Byn byn2 = new Byn("1.08");

        Assert.assertEquals(byn1, byn2);
    }

    @Test
    public void toStringMethod() {
        String expected = "1.08";
        Byn byn = new Byn(expected);
        Assert.assertEquals(expected, byn.toString());
    }
}