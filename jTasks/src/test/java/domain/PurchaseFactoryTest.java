package domain;

import models.Byn;
import models.Product;
import models.purchase.PercentDiscountPurchase;
import models.purchase.Purchase;
import org.junit.Assert;
import org.junit.Test;

public final class PurchaseFactoryTest {

    @Test
    public void createPriceDiscountPurchaseFromFactory() {
        String data = "bread;1.55;1;0.20";
        Purchase purchase = PurchaseFactory.fromCsv(data).get();
        Assert.assertNotNull(purchase);
    }

    @Test
    public void createPercentDiscountPurchaseFromFactory() {
        String data = "potato;0.80;2";
        Purchase purchase = PurchaseFactory.fromCsv(data).get();
        Assert.assertNotNull(purchase);
    }


    @Test
    public void createPurchaseFromFactory() {
        String data = "milk;1.31;2";
        Purchase purchase = PurchaseFactory.fromCsv(data).get();
        Assert.assertNotNull(purchase);
    }


    @Test
    public void objectTypeIsPercentDiscountPurchase() {
        String data = "potato;0.80;2;discount";
        Purchase purchase = PurchaseFactory.fromCsv(data).get();
        Assert.assertTrue(purchase instanceof PercentDiscountPurchase);
    }

    @Test
    public void objectTypeIsPurchase(){
        String data = "milk;1.31;2";
        Purchase purchase = PurchaseFactory.fromCsv(data).get();
        Assert.assertTrue(purchase instanceof Purchase);
    }

    @Test
    public void checkIdentityOfObjects(){
        String data = "milk;1.31;2";
        Purchase expected = new Purchase(new Product("milk", new Byn(131)), 2);
        Purchase actual = PurchaseFactory.fromCsv(data).get();
        Assert.assertEquals(expected, actual);
    }
}