package models;

import models.purchase.PercentDiscountPurchase;
import models.purchase.Purchase;

import java.util.List;

public class Receipt {
    private final List<Purchase> purchases;
    private final Card card;

    public Receipt(List<Purchase> purchases, Card card) {
        this.purchases = purchases;
        this.card = card;
    }

    public Byn getBaseCost() {
        return purchases.stream()
                .map(Purchase::getCost)
                .reduce(Byn::sum)
                .orElse(new Byn());
    }

    public Byn getProductDiscount() {
        return purchases.stream()
                .map(purchase -> purchase instanceof PercentDiscountPurchase ?
                        ((PercentDiscountPurchase) purchase).getDiscount() :
                        new Byn())
                .reduce(Byn::sum)
                .orElse(new Byn());
    }

    public Byn getTotalCost() {
        return getBaseCost().sub(getDiscountByCard());
    }

    public Byn getDiscountByCard() {
        return getBaseCost().multiply(card.getDiscountPercent() / 100.0);
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public Card getCard() {
        return card;
    }
}
