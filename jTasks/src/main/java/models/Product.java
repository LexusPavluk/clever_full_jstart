package models;

import exeptions.WrongArgumentException;

import java.util.Optional;

public final class Product {
    private final String name;
    private final Byn price;

    public Product(String name, Byn price) {
        this.name = notEmptyName(name)
                .orElseThrow(() -> new WrongArgumentException(name));
        this.price = new Byn(price);
    }

    public Product(String name, String price) {
        this(name, new Byn(price));
    }

    public Product(Product product) {
        this(product.name, product.price);
    }

    private static Optional<String> notEmptyName(String name) {
            return Optional.of(name)
                    .filter(n -> n.length() !=0);
    }

    public String getName() {
        return name;
    }

    public Byn getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return name + ';' + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return name.equals(product.name) &&
                price.equals(product.price);
    }

    @Override
    public int hashCode() {
        int hash = price.hashCode();
        hash = name.hashCode() + 31 * hash;
        return hash;
    }
}
