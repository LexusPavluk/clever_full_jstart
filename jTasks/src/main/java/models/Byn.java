package models;

import exeptions.NegativeArgumentException;
import exeptions.PatternArgumentException;
import exeptions.WrongArgumentException;

import java.util.Optional;
import java.util.regex.Pattern;

public final class Byn implements Comparable<Byn> {
    private static final int CENTS_DIVISIBILITY = 100;
    private static final Pattern PATTERN = Pattern.compile("\\d+\\.\\d{2}");

    private final int value;

    public Byn(int cents) {
        this.value = nonNegative(cents)
        .orElseThrow(() -> new NegativeArgumentException(cents));
    }

    public Byn(Byn byn) {
        this.value = Optional.of(byn.value)
                .orElseThrow(() -> new WrongArgumentException("Absent value of models.Byn"));
    }

    public Byn() {
        this.value = 0;
    }

    public Byn(int rubs, int coins) {
        this(getValidValues(rubs, coins)
                .orElseThrow(() -> new WrongArgumentException("" + rubs + ", " + coins)));
    }

    private static Optional<Integer> getValidValues(int rubs, int coins) {
        if (rubs < 0 || coins < 0 || coins > 99) {
            return Optional.empty();
        }
        return Optional.of(rubs * CENTS_DIVISIBILITY + coins);
    }

    public Byn(String value) {
        this(parseBynToCents(value).orElseThrow(() -> new PatternArgumentException(value)));
    }

    private static Optional<Integer> parseBynToCents(String value) {
        if (PATTERN.matcher(value).matches()) {
            String[] partsByn = value.split("\\.");
            return getValidValues(Integer.parseInt(partsByn[0]), Integer.parseInt(partsByn[1]));
        }
        return Optional.empty();
    }

    private static Optional<Integer> nonNegative(int value) {
            return Optional.of(value)
                    .filter(integer -> integer >= 0);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(Integer.toString(value))
                .reverse()
                .append(value > 9 ? "" : '0')
                .insert(2, '.')
                .reverse()
                .insert(0, value < 100 ? '0' : "");
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Byn)) return false;
        Byn byn = (Byn) o;
        return value == byn.value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public int compareTo(Byn o) {
        return value - o.value;
    }

    public Byn sum(Byn addend) {
        return new Byn(value + addend.value);
    }

    public Byn sub(Byn subtrahend) {
        if (value < subtrahend.value) {
            throw new ArithmeticException(value + " - " + subtrahend.value);
        }
        return new Byn(value - subtrahend.value);
    }

    public Byn multiply(double multiplier) {
        return new Byn((int) Math.round(value * multiplier));
    }

}
