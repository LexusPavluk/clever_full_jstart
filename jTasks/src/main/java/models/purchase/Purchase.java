package models.purchase;

import exeptions.NonPositiveArgumentException;
import models.Byn;
import models.Product;

import java.util.Objects;
import java.util.Optional;

public class Purchase {
    private final Product product;
    private final double quantity;

    public Purchase(Product product, double quantity) {
        this.product = product;
        this.quantity = positive(quantity).orElseThrow(() -> new NonPositiveArgumentException(quantity));
    }

    public Purchase(Product product, String quantity) {
        this(product, Double.parseDouble(quantity));
    }


    public Purchase(Purchase purchase) {
        this(purchase.product, purchase.quantity);
    }

    public Product getProduct() {
        return new Product(product);
    }

    public double getQuantity() {
        return quantity;
    }


    protected static Optional<Double> positive(double value) {
        return Optional.of(value)
                .filter(dbl -> dbl > 0);
    }

    public Byn getCost() {
        return product.getPrice().multiply(quantity);
    }

    protected String fieldsToString() {
        return product + ";" + quantity;
    }

    @Override
    public String toString() {
        return fieldsToString() + ";" + getCost();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Purchase)) return false;
        Purchase purchase = (Purchase) o;
        return product.equals(purchase.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product.hashCode(), quantity);
    }

}
