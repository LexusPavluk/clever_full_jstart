package models.purchase;

import models.Byn;
import models.Product;

public class PercentDiscountPurchase extends Purchase {
    private static final int DISCOUNT_ITEM_NUMBER = 5;
    private static final double PERCENT = 10.0;

    public PercentDiscountPurchase(Product product, double quantity) {
        super(product, quantity);
    }

    public PercentDiscountPurchase(Purchase purchase) {
        super(purchase);
    }

    public PercentDiscountPurchase(Product product, String quantity) {
        super(product, quantity);
    }

    @Override
    public Byn getCost() {
        return baseCost().sub(getDiscount());
    }

    private Byn baseCost() {
        return super.getCost();
    }

    public Byn getDiscount() {
        return getQuantity() > DISCOUNT_ITEM_NUMBER ?
                new Byn(baseCost().multiply(PERCENT / 100.0)) :
                new Byn();
    }

    protected String fieldsToString() {
        return super.fieldsToString() + ";" + PERCENT;
    }
}