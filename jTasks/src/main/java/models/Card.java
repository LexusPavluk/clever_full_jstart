package models;

import java.util.Objects;

public class Card {
    private final int id;
    private final int discountPercent;

    public Card(Integer id, Integer discount_percent) {
        this.id = id;
        this.discountPercent = discount_percent;
    }

    public int getId() {
        return id;
    }

    public int getDiscountPercent() {
        return discountPercent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;
        Card card = (Card) o;
        return id == card.id &&
                discountPercent == card.discountPercent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, discountPercent);
    }

    @Override
    public String toString() {
        return "Card: id=" + id +
                ", discount percent=" + discountPercent + "%";
    }
}
