package exeptions;

public class WrongArgumentException extends IllegalArgumentException {

    public WrongArgumentException(String dateString) {
        super(dateString);
    }

    @Override
    public String toString() {
        return "WrongArgumentException{" +
                "wrong date='" + super.getMessage() + '\'' +
                '}';
    }

}
