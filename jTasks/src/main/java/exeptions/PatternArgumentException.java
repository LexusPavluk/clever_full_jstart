package exeptions;

public class PatternArgumentException extends IllegalArgumentException {
    protected final String argument;

    public PatternArgumentException(String argument) {
        super();
        this.argument = argument;
    }

    @Override
    public String toString() {
        return "PatternArgumentException{" +
                "argument=" + argument +
                '}';
    }

}
