package exeptions;

public class NonPositiveArgumentException extends IllegalArgumentException {
    protected final Number argument;

    public NonPositiveArgumentException(Number argument) {
        super();
        this.argument = argument;
    }

    public NonPositiveArgumentException(String fieldName, int argument) {
        super(fieldName);
        this.argument = argument;
    }


    @Override
    public String toString() {
        return "NonPositiveArgumentException{" +
                super.getMessage() +
                " argument= " + argument +
                '}';
    }

}
