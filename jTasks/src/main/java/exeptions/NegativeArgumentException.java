package exeptions;

public class NegativeArgumentException extends NonPositiveArgumentException {

    public NegativeArgumentException(int argument) {
        super(argument);
    }

    @Override
    public String toString() {
        return "NegativeArgumentException{" +
                "argument=" + argument +
                '}';
    }
}
