import data.*;
import domain.ActionFactory;
import domain.MyRepository;
import domain.command.ActionCommand;
import models.Card;
import models.Product;

public class Runner {
    public static void main(String[] args) {
        // аргументы запуска Runner:
        // для прямой передачи данных из args:  2-3 5-6 12-1 18-2 25-4 30-8 card-123
        // для передачи данных из файла:  src/main/resources/purchases.csv
        //  GET запрос: http://localhost:8001/check?itemId=2-3&itemId=5-6&itemId=12-1&itemId=17-6&itemId=card-120

        DBase<Product> productBase = new ProductDataBase();
        DBase<Product> discountBase = new DiscountableProductsDB(productBase);
        DBase<Card> cardBase = new CardDataBase();
        IRepository repository = new MyRepository(productBase, discountBase, cardBase);

        ActionFactory actionFactory = new ActionFactory(repository);
        ActionCommand command = actionFactory.defineCommand(args);
        command.execute(args);

    }


}
