package data;

import domain.Constants;
import models.Card;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;

public class CardDataBase implements DBase<Card> {
    private final Map<Integer, Card> cardBase;


    {
        cardBase = Stream.iterate(1, n -> n + 1).limit(999)
                .collect(Collectors.toMap(
                        identity(),
                        i1 -> new Card(i1, new Random().nextInt(10))
                ));
        cardBase.put(0, Constants.DEFAULT_CARD);
    }

    @Override
    public Map<Integer, Card> getBase() {
        return cardBase;
    }

    public Optional<Card> read(int key) {
        return Optional.ofNullable(cardBase.get(key));
    }
}
