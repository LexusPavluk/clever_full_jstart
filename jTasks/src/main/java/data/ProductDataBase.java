package data;

import models.Byn;
import models.Product;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;

public class ProductDataBase implements DBase<Product>{
    private final Map<Integer, Product> productBase;

    {
        List<String> items = Arrays.asList("potato", "bread", "cheese", "milk",
                "hammer", "phone", "book",
                "spam", "eggs", "foo", "bar",
                "Barbie", "Ken", "shirt", "pants");

        productBase = Stream.iterate(1, n -> n + 1).limit(25)
                .collect(Collectors.toMap(
                        identity(),
                        i1 -> new Product(items.get(i1 % items.size()), randomByn())));
    }

    private static Byn randomByn() {
        return new Byn(randomInt(50), randomInt(100));
    }

    private static int randomInt(int scale) {
        return new Random().nextInt(scale);
    }

    @Override
    public Map<Integer, Product> getBase() {
        return productBase;
    }

    @Override
    public Optional<Product> read(int key) {
        return Optional.ofNullable(productBase.get(key));
    }

}
