package data;

import java.util.Map;
import java.util.Optional;

public interface DBase<T> {
    Map<Integer, T> getBase();
    Optional<T> read (int key);
}
