package data;

import models.Card;
import models.Product;

import java.util.Optional;

public interface IRepository {
    Optional<Product> getProduct(int key);
    boolean isDiscountProduct(Product product);
    Optional<Card> getCard(int key);

}
