package data;

import models.Product;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DiscountableProductsDB implements DBase<Product> {
    private Map<Integer, Product> discountProducts;

    public DiscountableProductsDB(DBase<Product> productDataBase) {
        fillDiscountDataBase(productDataBase);
    }

    private void fillDiscountDataBase(DBase<Product> productDataBase){
        discountProducts = productDataBase.getBase().entrySet().stream()
                .sorted(Comparator.comparing(entry -> entry.getValue().getPrice()))
                .limit(5)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }

    @Override
    public Map<Integer, Product> getBase() {
        return discountProducts;
    }

    @Override
    public Optional<Product> read(int key) {
        return Optional.ofNullable(discountProducts.get(key));
    }
}
