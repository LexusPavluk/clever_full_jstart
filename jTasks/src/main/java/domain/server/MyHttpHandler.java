package domain.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import data.IRepository;
import domain.Constants;
import domain.services.ReceiptPrinter;
import domain.services.ReceiptService;
import models.Receipt;
import org.apache.commons.text.StringEscapeUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class MyHttpHandler implements HttpHandler {

    private final IRepository repository;

    public MyHttpHandler(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String requestParamValue = null;

        if ("GET".equals(httpExchange.getRequestMethod())) {
            requestParamValue = handleGetRequest(httpExchange);
        }

        handleResponse(httpExchange, requestParamValue);

    }

    private String handleGetRequest(HttpExchange httpExchange) {
        String[] httpArgs =
                Arrays.stream(httpExchange.getRequestURI()
                        .toString().split(Constants.END_URL)[1].split(Constants.GET_DELIMITER))
                .map(s -> s.split(Constants.GET_ARG_NAME)[1])
                .toArray(String[]::new);
        ReceiptService service = new ReceiptService(repository);
        Receipt receipt = service.getReceiptFromArgs(httpArgs);
        ReceiptPrinter printer = new ReceiptPrinter(receipt);
        return printer.receiptPrint();
    }

    private void handleResponse(HttpExchange httpExchange, String requestParamValue) throws IOException {
        OutputStream outputStream = httpExchange.getResponseBody();
        StringBuilder htmlBuilder = new StringBuilder();

        htmlBuilder
//                .append("<h2>")
                .append(requestParamValue)
//                .append("</h2>")
                ;

        String htmlResponse = StringEscapeUtils.unescapeHtml4(htmlBuilder.toString());

        httpExchange.sendResponseHeaders(200, htmlResponse.length());

        outputStream.write(htmlResponse.getBytes());
        outputStream.flush();
        outputStream.close();
    }
}
