package domain.server;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyHttpServer {
    private static final Logger LOGGER = Logger.getLogger(MyHttpServer.class);
    private static final String HOST = "localhost";
    private static final int PORT = 8001;
    private static final String PATH = "/check";
    private static final int DOWNTIME_DELAY = 5;

    private HttpServer server;
    private ExecutorService threadPoolExecutor;
    private final HttpHandler handler;

    public MyHttpServer(HttpHandler handler) {
        this.handler = handler;
    }

    public void setServer() throws IOException {
        server = HttpServer.create(new InetSocketAddress(HOST, PORT), 0);

        threadPoolExecutor = Executors.newFixedThreadPool(5);
        server.createContext(PATH, handler);
        server.setExecutor(threadPoolExecutor);
        server.start();
        LOGGER.info("Server started on port " + PORT);
        dialogServerStop();
    }

    private void dialogServerStop() {
        Scanner scanner = new Scanner(System.in);
        String response;
        LOGGER.info("To stop server, type: 'y'..");
        do {
            response = scanner.nextLine();
        } while (!response.equals("y"));
        scanner.close();
        serverStop();
    }

    private void serverStop() {
        server.stop(DOWNTIME_DELAY);
        threadPoolExecutor.shutdown();
        LOGGER.info("Server stopped.");
    }
}
