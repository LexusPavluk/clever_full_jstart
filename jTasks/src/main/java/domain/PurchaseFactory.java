package domain;

import models.Product;
import models.purchase.PercentDiscountPurchase;
import models.purchase.Purchase;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

public class PurchaseFactory {
    private static final Logger LOGGER = Logger.getLogger(PurchaseFactory.class);

    private static final int NAME_INDEX = 0;
    private static final int PRICE_INDEX = 1;
    private static final int QTY_INDEX = 2;

    private PurchaseFactory() {
    }

    public static Optional<Purchase> fromCsv(String csvArgs) {
        try {
            String[] args = csvArgs.split(Constants.CSV_ARG_DELIMITER);

            Product product = new Product(args[NAME_INDEX], args[PRICE_INDEX]);
            double quantity = Double.parseDouble(args[QTY_INDEX]);
            Purchase prePurchase = new Purchase(product, quantity);
            PurchaseType purchaseType = Arrays.asList(args).contains(Constants.DISCOUNT) ?
                    PurchaseType.PERCENT_DISCOUNT_PURCHASE :
                    PurchaseType.PURCHASE;
            return Optional.of(purchaseType.getPurchase(prePurchase));
        } catch (Exception e) {
            LOGGER.warn("Invalid purchase data: ",e);
            return Optional.empty();
        }
    }

    enum PurchaseType {
        PURCHASE(Purchase::new),
        PERCENT_DISCOUNT_PURCHASE(PercentDiscountPurchase::new);

        private final Function<Purchase, Purchase> creator;

        PurchaseType(Function<Purchase, Purchase> creator) {
            this.creator = creator;
        }

        Purchase getPurchase(Purchase facade) {
            return creator.apply(facade);
        }

    }
}
