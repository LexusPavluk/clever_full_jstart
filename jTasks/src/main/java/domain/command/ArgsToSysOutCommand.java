package domain.command;

import data.IRepository;
import domain.services.DialogFileWriter;
import domain.services.ReceiptService;
import domain.services.ReceiptPrinter;
import models.Receipt;

public class ArgsToSysOutCommand extends AbstractCommand {

    public ArgsToSysOutCommand(IRepository repository) {
        super(repository);
    }

    @Override
    public void execute(String... purchaseArgs) {
        ReceiptService service = new ReceiptService(repository);
        Receipt receipt = service.getReceiptFromArgs(purchaseArgs);
        ReceiptPrinter printer = new ReceiptPrinter(receipt);
        String receiptPrint = printer.receiptPrint();
        System.out.println(receiptPrint);
        DialogFileWriter fileWriter = new DialogFileWriter();
        fileWriter.dialog(receiptPrint);
    }
}
