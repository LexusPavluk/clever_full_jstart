package domain.command;

import data.IRepository;

public abstract class AbstractCommand implements ActionCommand{
    protected final IRepository repository;

    public AbstractCommand(IRepository repository) {
        this.repository = repository;
    }
}
