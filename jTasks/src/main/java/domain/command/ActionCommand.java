package domain.command;

public interface ActionCommand {

    void execute(String... purchaseArgs);

}
