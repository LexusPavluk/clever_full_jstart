package domain.command;

import com.sun.net.httpserver.HttpHandler;
import data.IRepository;
import domain.server.MyHttpHandler;
import domain.server.MyHttpServer;
import org.apache.log4j.Logger;

import java.io.IOException;

public class ServerCommand extends AbstractCommand {
    private static final Logger LOGGER = Logger.getLogger(ServerCommand.class);

    private final HttpHandler handler;

    public ServerCommand(IRepository repository) {
        super(repository);
        handler = new MyHttpHandler(repository);
    }

    @Override
    public void execute(String... purchaseArgs) {
        // http://localhost:8001/check?itemId=2-3&itemId=5-6&itemId=12-1&itemId=17-6&itemId=card-120
        try {
            MyHttpServer server = new MyHttpServer(handler);
            server.setServer();
        } catch (IOException e) {
            LOGGER.warn("Server IO error: ", e);
        }

    }
}
