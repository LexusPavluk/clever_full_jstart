package domain.command;

import data.IRepository;
import domain.services.DialogFileWriter;
import domain.services.ReceiptPrinter;
import domain.services.ReceiptService;
import models.Receipt;

import java.io.IOException;

public class FileProcessingCommand extends AbstractCommand {

    public FileProcessingCommand(IRepository repository) {
        super(repository);
    }

    @Override
    public void execute(String... purchaseArgs) {
        ReceiptService service = new ReceiptService(repository);
        try {
            Receipt receipt = service.getReceiptFromFileName(purchaseArgs[0]);
            ReceiptPrinter printer = new ReceiptPrinter(receipt);
            String receiptPrint = printer.receiptPrint();
            System.out.println(receiptPrint);
            DialogFileWriter fileWriter = new DialogFileWriter();
            fileWriter.dialog(receiptPrint);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
