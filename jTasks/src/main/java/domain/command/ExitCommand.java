package domain.command;

public class ExitCommand implements ActionCommand {

    @Override
    public void execute(String... purchaseArgs) {
        System.exit(0);
    }
}
