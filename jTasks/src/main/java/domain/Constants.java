package domain;

import models.Card;

import java.util.regex.Pattern;

public class Constants {
    public static final String ARG_DELIMITER = "-";
    public static final String CSV_ARG_DELIMITER = ";";
    public static final String CARD = "card";
    public static final String DISCOUNT = "discount";

    public static final String GET_ARG_NAME = "itemId=";
    public static final String GET_DELIMITER = "\\&";
    public static final String END_URL = "\\?";

    public static final Pattern ARG_PATTERN = Pattern.compile("(\\d+|" + CARD + ")-\\d+");
    public static final Pattern PURCHASE_CSV_PATTERN =
            Pattern.compile("\\w*;\\d+\\.\\d{2};\\d+\\.?\\d*(;" + DISCOUNT + ")?");
    public static final Pattern CARD_CSV_PATTERN =
            Pattern.compile("(" + CARD + "-\\d+;\\d{1})");

    public static final String DEFAULT_OUTPUT_FILE_NAME = "src\\main\\resources\\receipt.txt";
    public static final Card DEFAULT_CARD = new Card(0,0);
}
