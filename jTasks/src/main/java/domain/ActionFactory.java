package domain;

import data.IRepository;
import domain.command.*;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Arrays;

public class ActionFactory {
    private static final Logger LOGGER = Logger.getLogger(ActionFactory.class);

    private final IRepository repository;

    public ActionFactory(IRepository repository) {
        this.repository = repository;
    }

    public ActionCommand defineCommand(String... args) {
        if (!isArgsPresent(args)) {
            LOGGER.info("Starting HTTP Server....");
            return new ServerCommand(repository);
        } else if (isReceiptArgs(args)) {
            LOGGER.info("Output receipt to console...");
            return new ArgsToSysOutCommand(repository);
        } else if (isFileNameArgs(args)) {
            LOGGER.info("Processing file to receipt...");
            return new FileProcessingCommand(repository);
        } else {
            return new ExitCommand();
        }
    }


    private static boolean isArgsPresent(String... args) {
        return args.length > 0;
    }

    private static boolean isReceiptArgs(String... args) {
        return Arrays.stream(args)
                .anyMatch(s -> Constants.ARG_PATTERN.matcher(s).matches());
    }

    private static boolean isFileNameArgs(String... args) {
        boolean isFileName = new File(args[0]).isFile();
        LOGGER.info("Is file name: " + args[0] + " - " + isFileName);
        return isFileName;
    }

}
