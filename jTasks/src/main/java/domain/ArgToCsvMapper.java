package domain;

import data.IRepository;

public class ArgToCsvMapper {
    private static final int PRODUCT_INDEX = 0;
    private static final int QTY_INDEX = 1;

    private final IRepository repository;

    public ArgToCsvMapper(IRepository repository) {
        this.repository = repository;
    }

    public String mapArg(String arg) {
        String[] args = arg.split(Constants.ARG_DELIMITER);

        return repository.getProduct(Integer.parseInt(args[PRODUCT_INDEX]))
                .map(product -> product.getName() + ";"
                            + product.getPrice().toString() + ";"
                            + Integer.parseInt(args[QTY_INDEX]) + ";"
                            + (repository.isDiscountProduct(product) ? ";discount" : "")
                )
                .orElse("");
    }

}
