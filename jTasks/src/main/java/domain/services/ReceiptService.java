package domain.services;

import data.IRepository;
import domain.ArgToCsvMapper;
import domain.Constants;
import domain.PurchaseFactory;
import models.Card;
import models.Receipt;
import models.purchase.Purchase;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReceiptService {

    private final IRepository repository;
    private final ArgToCsvMapper mapper;


    public ReceiptService(IRepository repository) {
        this.repository = repository;
        mapper = new ArgToCsvMapper(repository);
    }

    public Receipt getReceiptFromArgs(String... receiptArgs) {

        int cardIndex = receiptArgs.length - 1;
        String[] cardArgs = receiptArgs[cardIndex].split(Constants.ARG_DELIMITER);
        Card card = repository.getCard(Integer.parseInt(cardArgs[1])).orElse(Constants.DEFAULT_CARD);

        String[] purchasesArgs = Arrays.copyOf(receiptArgs, cardIndex);
        List<Purchase> purchases = Arrays.stream(purchasesArgs)
                .map(mapper::mapArg)
                .filter(s -> !s.isEmpty())
                .map(PurchaseFactory::fromCsv)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        return new Receipt(purchases, card);
    }

    public Receipt getReceiptFromFileName(String fileName) throws IOException {
        List<String> dataList = readDataFromFile(fileName);
        String zeroCardArgs = "0;0";

        Integer[] cardArgs = Arrays.stream(Arrays.stream(dataList.stream()
                .filter(s -> s.contains(Constants.CARD))
                .findAny().orElse(Constants.CARD + "-" + zeroCardArgs)
                .split(Constants.ARG_DELIMITER))
                .filter(s -> !s.contains(Constants.CARD))
                .findAny().orElse(zeroCardArgs)
                .split(Constants.CSV_ARG_DELIMITER))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);
        Card card = new Card(cardArgs[0], cardArgs[1]);

        List<Purchase> purchases = dataList.stream()
                .filter(s -> !s.contains(Constants.CARD))
                .map(PurchaseFactory::fromCsv)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        return new Receipt(purchases, card);

    }

    private List<String> readDataFromFile(String fileName) throws IOException {
        if (new File(fileName).exists()) {
            try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
                return lines
                        .filter(s -> Constants.PURCHASE_CSV_PATTERN.matcher(s).matches() ||
                                Constants.CARD_CSV_PATTERN.matcher(s).matches())
                        .collect(Collectors.toList());
            }
        }
        throw new IOException("Is not a file name: " + fileName);
    }


}
