package domain.services;

import java.io.*;
import java.util.regex.Pattern;

public class ReceiptToFileWriter {
    private static final Pattern PATTERN = Pattern.compile("\n");

    private final String receiptPrint;
    private final String fileName;

    public ReceiptToFileWriter(String receipt, String fileName) {
        this.receiptPrint = receipt;
        this.fileName = fileName;
    }

    public boolean writeReceiptToFile() {
        File file = new File(fileName);
        if (file.exists()) {
            try (PrintWriter printWriter = new PrintWriter(
                    new BufferedWriter(
                            new FileWriter(file,false)))){

                PATTERN.splitAsStream(receiptPrint)
                        .forEach(printWriter::println);

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
