package domain.services;

import models.Byn;
import models.Receipt;
import models.purchase.PercentDiscountPurchase;
import models.purchase.Purchase;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReceiptPrinter {

    private static final int[] TABS_WIDTH = {6, 25, 6, 9, 10};
    private static final String FORMAT =
            "%-" + TABS_WIDTH[0] + "s%-" +
                    TABS_WIDTH[1] + "s%" +
                    TABS_WIDTH[2] + "s%" +
                    TABS_WIDTH[3] + "s%" +
                    TABS_WIDTH[4] + "s";
    private static final int RECEIPT_WIDTH = Arrays.stream(TABS_WIDTH).sum();
    private static final String HEADER = "CASH RECEIPT";
    private static final String MARKET = "Supermarket #123";
    private static final String ADDRESS = "12, Milkyway GALAXY / Earth";
    private static final String PHONE = "Tel: 123-456-7890";
    private static final String BLOCK_DELIMITER = Stream
            .iterate("=", s -> s)
            .limit(RECEIPT_WIDTH)
            .collect(Collectors.joining());
    private static final String CASHIER = "Cashier #1520";
    private static final String DATE_TIME = "Date/Time: " +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd / HH:mm:ss"));
    private static final String COLUMN_HEADER = String.format(
            FORMAT, "QTY", "DESCRIPTION", "PRICE", "TOTAL", "DISCOUNT");
    private static final String PURCHASES_COST = "Purchases cost:";
    private static final String DISCOUNT_BY_PRODUCTS = "    including discount:";
    private static final String DISCOUNT_BY_CARD = "Card discount:";
    private static final String TOTAL_DISCOUNT = "Total discount:";
    private static final String TOTAL_COST = "TOTAL:";

    private final Receipt receipt;

    public ReceiptPrinter(Receipt receipt) {
        this.receipt = receipt;
    }

    public String receiptPrint() {
        return String.join("\n", getReceiptLines());
    }

    private List<String> getReceiptLines() {
        List<String> builder = new ArrayList<>(receiptHeader());
        builder.addAll(receiptBody());
        builder.addAll(receiptFooter());
        return builder;
    }

    private List<String> receiptHeader() {
        List<String> headerBuilder = new ArrayList<>();
        headerBuilder.add(centerAligner(HEADER));
        headerBuilder.add(centerAligner(MARKET));
        headerBuilder.add(centerAligner(ADDRESS));
        headerBuilder.add(centerAligner(PHONE));
        headerBuilder.add(CASHIER);
        headerBuilder.add(DATE_TIME);
        headerBuilder.add(BLOCK_DELIMITER);
        return headerBuilder;
    }

    private List<String> receiptBody() {
        List<String> bodyBuilder = new ArrayList<>();
        bodyBuilder.add(COLUMN_HEADER);
        bodyBuilder.addAll(receipt.getPurchases().stream()
                .map(ReceiptPrinter::purchaseToFormattedString)
                .collect(Collectors.toList()));
        bodyBuilder.add(BLOCK_DELIMITER);
        return bodyBuilder;
    }

    private List<String> receiptFooter() {
        List<String> footerBuilder = new ArrayList<>();
        footerBuilder.add(resultFormatter(PURCHASES_COST, receipt.getBaseCost()));
        footerBuilder.add(resultFormatter(DISCOUNT_BY_PRODUCTS, receipt.getProductDiscount()));
        if (receipt.getCard().getId() != 0) {
            footerBuilder.add(receipt.getCard().toString());
            footerBuilder.add(resultFormatter(DISCOUNT_BY_CARD, receipt.getDiscountByCard()));
        }
        footerBuilder.add(resultFormatter(TOTAL_DISCOUNT,
                receipt.getProductDiscount().sum(receipt.getDiscountByCard())));
        footerBuilder.add(resultFormatter(TOTAL_COST, receipt.getTotalCost()));
        return footerBuilder;
    }

    private static String purchaseToFormattedString(Purchase purchase) {
        double qty = purchase.getQuantity();
        String productName = purchase.getProduct().getName();
        Byn price = purchase.getProduct().getPrice();
        Byn cost = purchase.getCost();
        String discount = purchase instanceof PercentDiscountPurchase ?
                ((PercentDiscountPurchase) purchase).getDiscount().toString() :
                "";
        return String.format(FORMAT, qty, productName,
                price.toString(), cost.toString(), discount);
    }

    private String resultFormatter(String header, Byn result) {
        String format = "%-" + Arrays.stream(TABS_WIDTH).limit(3).sum() + "s"
                + "%" + TABS_WIDTH[3] + "s";
        return String.format(format, header, result.toString());
    }

    private String centerAligner(String s) {
        String format = "%" + ((RECEIPT_WIDTH - s.length()) / 2 + s.length()) + "s";
        return String.format(format, s);
    }

}
