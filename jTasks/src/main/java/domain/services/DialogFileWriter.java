package domain.services;

import domain.Constants;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DialogFileWriter {
    private static final Logger LOGGER = Logger.getLogger(DialogFileWriter.class);
    private final Scanner scanner = new Scanner(System.in);

    public void dialog(String receiptPrint) {
        String response;
        do {
            LOGGER.info("Will write receipt to file? Type: 'y', or 'n' if not..");
            response = scanner.nextLine();
            if (response.equals("y")) {
                ReceiptToFileWriter fileWriter = new ReceiptToFileWriter(receiptPrint, getFilePath());
                if (fileWriter.writeReceiptToFile()) {
                    LOGGER.info("File successfully written");
                    break;
                }
                LOGGER.info("File recording failed, please try again....");
            }
        } while (!response.equals("n"));
        scanner.close();
    }

    private String getFilePath() {
        String response;
        LOGGER.info("Will write receipt to default path: " + Constants.DEFAULT_OUTPUT_FILE_NAME + " ?");
        while(true) {
            LOGGER.info("Type: 'y', or other file path to write...");
            response = scanner.nextLine();
            if (response.equals("y")) {
                return Constants.DEFAULT_OUTPUT_FILE_NAME;
            } else {
                if (isFileName(response)) {
                    return response;
                } else {
                    LOGGER.info(response + "is not file name...");
                }
            }
        }
    }

    private boolean isFileName(String fileName) {
        File file = new File(fileName);
        try (FileWriter fileWriter = new FileWriter(file)){
            fileWriter.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.exists();
    }

}
