package domain;

import data.*;
import models.Card;
import models.Product;

import java.util.Optional;

public class MyRepository implements IRepository {

    private final DBase<Product> productDataBase;
    private final DBase<Product> discountableProductDataBase;
    private final DBase<Card> cardDataBase;

    public MyRepository(DBase<Product> productDataBase, DBase<Product> discountableProductDataBase, DBase<Card> cardDataBase) {
        this.productDataBase = productDataBase;
        this.discountableProductDataBase = discountableProductDataBase;
        this.cardDataBase = cardDataBase;
    }

    @Override
    public Optional<Product> getProduct(int key) {
        return productDataBase.read(key);
    }

    @Override
    public boolean isDiscountProduct(Product product) {
        return discountableProductDataBase.getBase().containsValue(product);
    }

    @Override
    public Optional<Card> getCard(int key) {
        return cardDataBase.read(key);
    }


}
